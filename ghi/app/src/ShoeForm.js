import React, {useEffect, useState} from "react";

function ShoeForm() {
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);
    const [hasCreatedShoe, setHasCreatedShoe] = useState(false);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.manufacturer = manufacturer;
            data.modelName = modelName;
            data.color = color;
            data.pictureUrl = pictureUrl;
            data.bin = bin;


        const shoeUrl = 'http://localhost:8100/api/bins';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const shoeResponse = await fetch(shoeUrl, fetchOptions);

            if (shoeResponse.ok) {
                setManufacturer('');
                setModelName('');
                setColor('');
                setPictureUrl('');
                setBin('');
                setHasCreatedShoe(false);
            } else {
                // Handle the case when the response is not OK (e.g., display an error message)
                console.error('Failed to create shoe:', shoeResponse.statusText);
            }
        } catch (error) {
            // Handle fetch errors (e.g., network issues)
            console.error('Fetch error:', error);
        }
    };

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleChangeModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (bin.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasCreatedShoe) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none'
    }
    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form className={formClasses} onSubmit={handleSubmit} id="create-shoe-form">
                                <h1 className="card-title">It's Shoe Time!</h1>
                                <p className="mb-3">
                                    Please choose which bin
                                    you'd like to store the shoe in.
                                </p>
                                <div className={spinnerClasses} id="loading-location-spinner">
                                    <span className="visually-hidden">Loading...</span>
                                </div>

                                <p className="mb-3">
                                Please enter the shoe details.
                                </p>
                                    <div className="row">
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleChangeManufacturer} required placeholder="Manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control" />
                                            <label htmlFor="name">Manufacturer</label>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleChangeModelName} required placeholder="Model" type="text" id="model" name="model" className="form-control" />
                                            <label htmlFor="name">Model</label>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleChangeColor} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="name">Color</label>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleChangePictureUrl} required placeholder="Pic_Url" type="text" id="pic_url" name="pic_url" className="form-control" />
                                            <label htmlFor="name">Picture URL</label>
                                            </div>
                                        <div className="row">
                                            <div className="mb-3">
                                            <select onChange={handleChangeBin} name="bin" id="bin" className={dropdownClasses} required>
                                            <option value="">Choose a bin</option>
                                            {bins.map(bin => {
                                            return (
                                                <option key={bin.import_href} value={bin.import_href}>{bin.closet_name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">Store that shoe!</button>
                            </form>
                            <div className={messageClasses} id="success-message">
                                Congratulations! Your shoe has been created!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
