
import { useState, useEffect} from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([])

    const getData = async ()=> {
        const response = await fetch ('http://localhost:8080/api/shoes');
        if (response.ok) {
            const {shoes} = await response.json();
            setShoes(shoes);

        } else{
            console.error('An error occured fetching the data')
        }
    }

    const handleDelete = (shoesId) => {
        fetch(`http://localhost:8080/api/shoes/${shoesId}/`, { method: 'DELETE' })
            .then(() => {
                setShoes(shoes.filter(shoes => shoes.id !== shoesId));
            })
            .catch(error => console.error('Error deleting shoe:', error));
        };


    useEffect(()=> {
        getData()
    }, []);

    return (
        <div className="column">
        <div className="row">
            <h1>Current Shoes</h1>

            <table className="table">
            <thead>
                <tr>
                <th>Manufacturer</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                return (
                    <tr key={shoe.id}>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.bin.closet_name }</td>
                    <td>
                            <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                        </td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
        </div>
    );
}

export default ShoesList;
