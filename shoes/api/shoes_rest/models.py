from django.db import models
from django.urls import reverse

# Create your models here.

# manufacturer, model name, color, url for picture, bin in wardrobe


class BinVO(models.Model):
    import_href = models.CharField(max_length=200)
    id = models.PositiveSmallIntegerField(primary_key=True, null=False)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


class Shoes(models.Model):

    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("show_shoe", kwargs={"pk": self.pk})

    class Meta:
        ordering = ()
