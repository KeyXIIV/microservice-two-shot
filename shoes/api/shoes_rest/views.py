from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Shoes, BinVO
from common.json import ModelEncoder
import json

# Create your views here.


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url"
        "id",
    ]

    def get_extra_data(self, o):
        return {"location": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )

# show_shoe view


@require_http_methods(["GET", "PUT", "DELETE"])
def shoe_detail(request, pk):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    # else:
    #     content = json.loads(request.body)
    #     try:
    #         if "shoes" in content:
    #             shoes = Shoes.objects.get(id=content["shoes"])
    #             content["shoes"] = shoes
    #     except Shoes.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Invalid shoe id"},
    #             status=400
    #         )

    #     Shoes.objects.filter(id=pk).update(**content)
    #     shoes = Shoes.objects.update(**content)
    #     return JsonResponse(
    #         shoes,
    #         encoder=ShoeDetailEncoder,
    #         safe=False
    #     )
