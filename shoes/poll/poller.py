import django
import os
import sys
import time
import json
import requests
# this might be wrong, I copied the models file from shoes



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()
from shoes_rest.models import BinVO
# Import models from shoes_rest, here.


def get_bins():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={"closet_name": bin["closet_name"], "bin_number": bin["bin_number"], "bin_size": bin["bin_number"],
            "id": bin["id"]},
        )


def poll():
    while True:
        try:
            # Write your polling logic, here
            get_bins()
            print("Bins updated successfully.")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
