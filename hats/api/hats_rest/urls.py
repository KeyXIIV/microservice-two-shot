from django.urls import path
from views import hat_list, hatdetail

urlpatterns = [
    path("api/hats", hat_list, name="hat_list"),
    path("api/hats/<int:pk>/", hatdetail, name="hatdetail"),
]
