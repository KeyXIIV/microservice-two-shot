from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO
# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class ShowListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id"
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def hat_list(request):
    if request.method == "GET":
        hat = Hat.objects.all()
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def hatdetail(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    # else:
    #     content = json.loads(request.body)
        # try:
        #     if "hat" in content:
        #         hat = Hat.objects.get(id=content["hat"])
        #         content["hat"] = hat
        # except Hat.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid hat id"},
        #         status=400
        #     )

        # Hat.objects.filter(id=pk).update(content)
        # hat = Hat.objects.update(**content)
        # return JsonResponse(
        #     hat,
        #     encoder=HatDetailEncoder,
        #     safe=False
        # )
