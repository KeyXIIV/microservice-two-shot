from django.db import models
from django.urls import reverse
# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    id = models.PositiveSmallIntegerField(primary_key=True, null=False)
    closet_name = models.CharField(max_length=100, null=True)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=200, null=True)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)  # consider coming back to see if i can implent some sort of swatch display
    picture_url = models.URLField(max_length=500)  # check and make sure that URLField can render image urls
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("show_hat", kwargs={"pk": self.pk})

    class Meta:
        ordering = ()
