# Wardrobify

Team:

* Kaitlyn Padermos - Shoes
* Key Gomez - Hats


## Design

## Shoes microservice

This microservice will show a shoe tracker to track its model name, color, a picture and the bin that the shoe exists in. It will have a way to show a list of shoes, create a new shoe and delete a shoe. This will all be accessible through navigation links on the main page of the application.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I will create RESTful APIs that will be able to show a list of hats, create a new hat, and delete a hat. I will display those using React components that will be able to show a list of hats and their details and also show a form to create a new hat. I will create routes to my components using the existing navagation links haha lol


## How to Run this App
It is important to make sure you have Docker, Git and Django.

1. Fork the Repository: https://gitlab.com/KeyXIIV/microservice-two-shot/-/tree/main?ref_type=heads

2. Clone the forked repository onto your local computer using the git clone command

3. Build and run the project using Docker with the commands:
docker volume create beta-data
docker-compose build
docker-compose up

after doing so make sure your containers are running
you can view the project in the browser: http://localhost:3000/

## Diagram
This project is made up of the microservices:
Wardrobe
Shoes
Hats

![Alt text](<image (1).png>)

## API Documentation

### URLs and Ports
Hats uses the port 8090
The url for hats will be http://localhost:8090/api/hats
include an id number to delete or update hats

Shoes uses the port 8080
The url for shoes will be http://localhost:8080/api/shoes
include an id number to delete or update shoes

### Shoe API
Shoe API is built to store, create and update data about shoes in a bin, which is then stored in your wardrobe. To do so it makes use of the value object  "BinVO" to get information on where a specific shoe is located from the Wardrobe API. The model for "BinVO" is in models.py, which also has the "Shoe" model that is used to contain the properties of: manufacturer, model name, color, a picture url, and, using the BinVO model, bin.

These models are then used by the views in views.py to know what data should be displayed. It also allows for the creation of new sheoss, the updating of properties of an existing shoe and the deletion of an existing shoe.


### Hats API

Hats API is built to store, create and update data about hats in a location, which is then stored in your wardrobe. To do so it makes use of the value object  "LocationVO" to get information on where a specific hat is located from the Wardrobe API. The model for "LocationVO" is in models.py, which also has the "Hat" model that is used to contain the properties of: fabric, style name, color, a picture url, and, using the LocationVO model, location.

These models are then used by the views in views.py to know what data should be displayed. It also allows for the creation of new hats, the updating of properties of an existing hat and the deletion of an existing hat.

## Value Objects
Hats contains the Value object "LocationVO"

Shoes contains the Value object "BinVO"
